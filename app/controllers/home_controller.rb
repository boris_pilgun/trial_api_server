class HomeController < ApplicationController

  before_filter :restrict_access, :only => [:api_result]

  respond_to :json

  #all below uses just for manual testing of some abstract API
  def api_result
    respond_with do |format|
      format.json {render :json => Principal.load_all_from_file}
    end
  end

  private

  def restrict_access
    head :unauthorized unless request.env['HTTP_API_KEY'].to_s.eql?("goodkey")
  end

end