class Principal

  extend PrincipalsHandler

  attr_accessor :id, :name, :supplier_guid

  def initialize(attributes = {})
    attributes.each do |name, value|
      send("#{name}=", value)
    end
  end

  def self.load_all_from_remote
    load_from_remote
  end

  def self.load_all_from_file
    load_from_file
  end

end