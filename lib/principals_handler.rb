module PrincipalsHandler

  def load_from_file
    json = ActiveSupport::JSON.decode(File.read('db/data.json'))
    principals = []
    json['principals'].each do |principal_hash|
      principals << Principal.new(principal_hash)
    end if json['principals']
    principals
  end

end